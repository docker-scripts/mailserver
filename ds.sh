#!/bin/bash

cmd_start() {
    docker-compose up -d
}

cmd_stop() {
    docker-compose stop
}

cmd_restart() {
    docker-compose restart
}

cmd_shell() {
    docker-compose exec -u root mail bash
}

cmd_remove() {
    docker-compose down
    rm -f /etc/cron.d/"$(echo $(basename $(pwd)) | tr . -)"-copy-ssl-cert
}

cmd_make() {
    # customize mailserver.env
    ds set OVERRIDE_HOSTNAME $HOSTNAME
    ds set ONE_DIR 1
    ds set POSTMASTER_ADDRESS postmaster@$MAILDOMAIN
    ds set SPOOF_PROTECTION 1
    ds set ENABLE_SRS 1
    ds set ENABLE_FAIL2BAN 1
    ds set SSL_TYPE letsencrypt
    ds set POSTFIX_INET_PROTOCOLS ipv4
    
    # create docker-compose.yaml
    cat <<EOF > docker-compose.yaml
version: '3.8'

networks:
  ds:
    external:
      name: $NETWORK

services:
  mail:
    container_name: $CONTAINER
    image: $IMAGE
    restart: unless-stopped
    cap_add:
      - NET_ADMIN
    volumes:
      - ./data:/var/mail
      - ./state:/var/mail-state
      - ./logs:/var/log/mail
      - ./config/:/tmp/docker-mailserver/
      - ./sslcert/:/etc/letsencrypt/live/$HOSTNAME/
      - ./:/host/
    env_file:
      - mailserver.env
    ports:
      - "25:25"
      - "465:465"
      - "587:587"
      - "143:143"
      - "993:993"
      - "110:110"
      - "995:995"
      - "4190:4190"
    hostname: $HOSTNAME
    domainname: $MAILDOMAIN
    networks:
      ds:
        aliases:
          - $HOSTNAME
EOF

    # start the container
    docker-compose up -d

    # create an email account for postmaster
    ds setup email add postmaster@$MAILDOMAIN $POSTMASTER_PASSWD
    
    # make some configurations
    ds config
}
