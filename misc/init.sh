### run custom init steps

# make sure that docker-compose is installed
if ! hash docker-compose 2>/dev/null; then
    apt install --yes docker-compose
fi

# get config files and scripts from 'tomav/docker-mailserver'
wget -q https://raw.githubusercontent.com/tomav/docker-mailserver/master/mailserver.env
wget -q https://raw.githubusercontent.com/tomav/docker-mailserver/master/setup.sh
chmod a+x ./setup.sh

# create an example customization script, as explained at:
# https://github.com/tomav/docker-mailserver#custom-user-changes--patches
mkdir -p config
cat <<EOF config/user-patches.sh
#! /bin/bash

# ! THIS IS AN EXAMPLE !

# If you modify any supervisord configuration, make sure
# to run "supervisorctl update" afterwards.

set -euo pipefail
echo 'user-patches.sh started'

# . . .

echo 'user-patches.sh finished successfully'
EOF
chmod +x config/user-patches.sh
