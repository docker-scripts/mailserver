#!/bin/bash
# Customize LDAP configuration.  The structure of the data on my LDAP
# server is different from what is expected by the mailserver, so the
# settings on 'mailserver.env' are not sufficient to make a proper
# configuration.

source /host/settings.sh

rm -f /etc/postfix/{ldap-groups.cf,ldap-domains.cf}
postconf \
    "virtual_mailbox_domains = /etc/postfix/vhost" \
    "virtual_alias_maps = ldap:/etc/postfix/ldap-aliases.cf texthash:/etc/postfix/virtual" \
    "smtpd_sender_login_maps = ldap:/etc/postfix/ldap-users.cf"

sed -i /etc/postfix/ldap-users.cf \
    -e '/query_filter/d' \
    -e '/result_attribute/d' \
    -e '/result_format/d'
cat <<EOF >> /etc/postfix/ldap-users.cf
query_filter = (uid=%u)
result_attribute = uid
result_format = %s@$MAILDOMAIN
EOF

sed -i /etc/postfix/ldap-aliases.cf \
    -e '/domain/d' \
    -e '/query_filter/d' \
    -e '/result_attribute/d'
cat <<EOF >> /etc/postfix/ldap-aliases.cf
domain = $MAILDOMAIN
query_filter = (uid=%u)
result_attribute = mail
EOF

postfix reload
