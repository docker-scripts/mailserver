cmd_set_help() {
    cat <<EOF
    ds set VARIABLE [value]
        Set/unset the value of VARIABLE on 'mailserver.env'

EOF
}

cmd_set() {
    local var=$1
    local val=$2

    [[ -z $var ]] && echo -e "Usage:\n$(cmd_set_help)" && exit 1

    sed -i mailserver.env \
        -e "/^$var/ c $var=$val"
}
