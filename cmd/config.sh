cmd_config() {
    ds inject debian-fixes.sh

    ds @revproxy get-ssl-cert $HOSTNAME
    ds copy-ssl-cert
    _create_cron_job_to_copy_ssl_cert
}


# create a cron job that copies the ssl cert from revproxy once a week
_create_cron_job_to_copy_ssl_cert() {
    local dir=$(basename $(pwd))
    mkdir -p /etc/cron.d
    cat <<EOF > /etc/cron.d/"$(echo $dir | tr . -)"-copy-ssl-cert
# copy the ssl cert @$dir each week
0 0 * * 0  root  bash -l -c "ds @$dir copy-ssl-cert &> /dev/null"
EOF
}
